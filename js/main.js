//Constantes

const btnProductos = document.getElementById('btn_productos'), 
      grid =                  document.getElementById('grid'),
      categori_juego =        document.getElementById('categori_juegos'),
      categori_accesorios =   document.getElementById('categori_accesorios'),
      categori_periferico =   document.getElementById('categori_perifericos'),
      categori_merch =        document.getElementById('categori_merch'),
      sub_juego =             document.getElementById('sub_juegos'),
      sub_accesorios =        document.getElementById('sub_accesorios'),
      sub_perifericos =       document.getElementById('sub_perifericos'),
      sub_merch =             document.getElementById('sub_merch');
      esMovil = comSiEsMovil();

function comSiEsMovil(){
     if (window.innerWidth <= 800){
         return true} else{return false}
};

btnProductos.addEventListener("mouseover", function(){
    if(!comSiEsMovil()){
         grid.classList.add("active");
    }
});

grid.addEventListener("mouseleave", function(){
    if(!comSiEsMovil()){
         grid.classList.remove("active");
    }
});

/***********************************/
/*  Desplegables de categorías    */
/*********************************/

categori_juegos.addEventListener("mouseover", function(){
    if(!comSiEsMovil()){
         sub_juegos.classList.add("active")
         sub_accesorios.classList.remove("active")
         sub_perifericos.classList.remove("active")
         sub_merch.classList.remove("active")
    }
});

categori_perifericos.addEventListener("mouseover", function(){
    if(!comSiEsMovil()){
         sub_perifericos.classList.add("active");
         sub_juegos.classList.remove("active")
         sub_accesorios.classList.remove("active")
         sub_merch.classList.remove("active")
    }
});

categori_accesorios.addEventListener("mouseover", function(){
    if(!comSiEsMovil()){
         sub_accesorios.classList.add("active");
         sub_juegos.classList.remove("active")
         sub_perifericos.classList.remove("active")
         sub_merch.classList.remove("active")
    }
});

categori_merch.addEventListener("mouseover", function(){
    if(!comSiEsMovil()){
         sub_merch.classList.add("active");
         sub_juegos.classList.remove("active")
         sub_perifericos.classList.remove("active")
         sub_accesorios.classList.remove("active")
    }
});

